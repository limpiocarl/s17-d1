// ==== create array ====

// use an array literal - array literal

// e.g.

// array literal - (recommended)
const array1 = ["eat", "code", "sleep"];
console.log(array1);

// new keyword
const array2 = new Array("eat", "code", "sleep");
console.log(array2);

// empty array
const myList = [];

const numArray = [2, 3, 4, 5];

// array of strings
const stringArray = ["eat", "code", "sleep"];

// array of mixed data (not recommended) - data inside an array should be related
const newData = ["work", 1, true];

const newData1 = [
        { "task": "exercise" },
        [1, 2, 3],
        function hello() {
                console.log("Hi, I am an array.");
        }
]
console.log(newData1);

// mini-activity

let toGoPlaces = ['New York', 'Greece', 'Egypt', 'Japan', 'Korea', 'Greenland', 'Paris']

console.log(toGoPlaces[0]);
console.log(toGoPlaces);

console.log('===== array manipulation ======')

// ==== array manipulation ====

// add element to an array - push() add element at the end of the array

let dailyActivities = ['eat', 'code', 'sleep'];
dailyActivities.push('exercise');
console.log(dailyActivities);

// unshift() add element at the beginning of the array

dailyActivities.unshift('play');
console.log(dailyActivities);

dailyActivities[2] = 'grind';
console.log(dailyActivities);

// mini-activity

toGoPlaces[0] = 'BGNHS';
toGoPlaces[toGoPlaces.length - 1] = 'Silay';

console.log(toGoPlaces);
console.log(toGoPlaces[0]);
console.log(toGoPlaces[toGoPlaces.length - 1]);

// adding items in an array without using methods

let array = [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array);

array[1] = 'Tifa Lockhart';
console.log(array[1]);

array[array.length - 1] = 'Aerith Gainsborough';
console.log(array);

array[array.length] = 'Vincent Valentine';
console.log(array);

// ==== array methods ====
// manipulate array with pre-determined JS Functions
// mutators - these array methods usually change the original array

let array3 = ['Juan', 'Pedro', 'Jose', 'Andres'];

// without method

array3[array.length] = 'Francisco';
console.log(array3);

// .push() - allows us to add an element at the end of the array

array3.push('Andres');
console.log(array3);

// .unshift() - allows us to add an element at the start of the array

array3.unshift('Simon');
console.log(array3);

// .pop() - allows us to delete or remove the last element of the array

array3.pop();
console.log(array3);

// .pop() also able to return the element we removed

console.log(array3.pop());
console.log(array3);

let removedItem = array3.pop();
console.log(array3);
console.log(removedItem);

// .shift() return the item we removed

let removedItemShift = array3.shift();
console.log(array3);
console.log(removedItemShift);

// mini-activity

console.log('==== mini-activity ====');

let removedFirstItem = array3.shift();
console.log(array3);
console.log(removedFirstItem);

let removedLastItem = array3.pop();
console.log(array3);
console.log(removedLastItem);

array3.unshift('George');
console.log(array3);

array3.push('Michael');
console.log(array3);

// .sort() - by default, it will us to sort our items in ascending order

let numArray1 = [3, 2, 1, 6, 9, 8, 7];
numArray1.sort();
console.log(numArray1);

// .sort() converts all items into strings and then arranged items accordingly

let numArray2 = [30, 54, 768, 400, 983, 67];
numArray2.sort((a, b) => a - b); // arrow function
console.log(numArray2);

// ascending sort per number's value

numArray2.sort(function (a, b) {
        return a - b;
})
console.log(numArray2);

// descending sort

numArray2.sort(function (a, b) {
        return b - a;
})
console.log(numArray2);

let arrayStr = ['Mary', 'Zen', 'Jaime', 'Elaine'];
arrayStr.sort(function (a, b) {
        return b - a;
})
console.log(arrayStr);

// .reverse() - reversed the order of the items

arrayStr.sort();
console.log(arrayStr);

arrayStr.sort().reverse();
console.log(arrayStr);

// splice() - allows us to remove and add elements from a given index.
// syntax: array.splice(startingIndex, numberOfItemsToBeDeleted, elementsToAdd)

let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];

lakersPlayers.splice(0, 0, 'Caruso');
console.log(lakersPlayers);

lakersPlayers.splice(0, 1,);
console.log(lakersPlayers);

lakersPlayers.splice(0, 3,);
console.log(lakersPlayers);

lakersPlayers.splice(1, 1);
console.log(lakersPlayers);

lakersPlayers.splice(1, 0, 'Gasol', 'Fisher');
console.log(lakersPlayers);

// non-mutators

// slice()

let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];

computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
console.log(computerBrands);

let newBrands = computerBrands.slice(1, 3);
console.log(computerBrands);
console.log(newBrands);

let fonts = ['Times New Roman', 'Comic San MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];

let newFontSet = fonts.slice(1, 4);
console.log(newFontSet);
console.log(fonts);

// mini-activity

console.log('==== mini-activity ====');

let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];

let microsoft = videoGame.slice(3);
let nintendo = videoGame.slice(2, 4);

console.log(microsoft);
console.log(nintendo);

// .toString() - convert the array into a single value as a string but each item will be separated by a comma

//syntax: array.toString()

let sentence = ['I', 'like', 'JavaScript', '.', 'It', 'is', 'fun', '.'];

let sentenceString = sentence.toString();
console.log(sentence);
console.log(sentenceString);

// .join() - converts the array into a single value as a string but separator can be specified
// syntax: array.join(separator)

let sentence1 = ['My', 'favorite', 'food', 'is', 'chicken.',];

let sentenceString1 = sentence1.join(' ');
console.log(sentenceString1);

// mini-activity

console.log('===== mini activity =====');

let charArr = ["x", ".", "/", "2", "j", "M", "a", "r", "t", "i", "n", "J", "m", "M", "i", "g", "u", "e", "l", "f", "e", "y"];

let charArr1 = charArr.slice(5, 11);

let name1 = charArr1.join('');
console.log(name1);

let charArr2 = charArr.slice(13, 19);

let name2 = charArr2.join('');
console.log(name2);

// .concat() - it combines 2 or more arrays without affecting the original array
// syntax: array.concat(array1, array2)

let tasksFriday = ['drink HTML', 'eat JavaScript'];
let tasksSaturday = ['inhale CSS', 'breath BootStrap'];
let tasksSunday = ['Get Git', 'Be Node'];

let weekendTasks = tasksFriday.concat(tasksSaturday, tasksSunday);
console.log(weekendTasks);

// accessors 
// Methods that allow us to access our array.
// indexOf()
// - finds the index of the given element/item when it is first founc from the left.

let batch131 = [
        'Paolo',
        'Jamir',
        'Jed',
        'Ronel',
        'Jayson'
];

console.log(batch131.indexOf('Jed'));

// lastIndexof()
// - finds the index of the given element/item when it is last found from the right

console.log(batch131.lastIndexOf('Jamir'));

// mini-activity

console.log('===== mini activity =====');

let carBrands = ['BMW', 'Dodge', 'Maserati', 'Porsche', 'Chevrolet', 'Ferrari', 'GMC', 'Porsche', 'Mitsubishi', 'Toyota', 'Volkswagen', 'BMW']

function indexFinder(indexBrand, lastIndexBrand) {
        console.log(carBrands.indexOf(indexBrand), carBrands.lastIndexOf(lastIndexBrand));
}
indexFinder('Porsche', 'Porsche');

// ====== iterator methods ======

// - these methods iterate over the items in an array much like loop

let avengers = [
        'Hulk',
        'Black Widow',
        'Spider-man',
        'Iron man',
        'Captain America'
];

// forEach()

// - similar to for loop but is used on arrays. it will us to iterate over each time in an array and even add instruction per iteration

// anonymous function within forEach will be received each and every item in an array

avengers.forEach(function (avengers) {
        console.log(avengers);
})
console.log(avengers);

let marvelHeroes = [
        'Moon Knight',
        'Jessica Jones',
        'Deadpool',
        'Cyclops'
]

marvelHeroes.forEach(function (hero) {
        if (hero !== 'Cyclops' && hero !== 'Deadpool') {
                avengers.push(hero);
        }
})
console.log(avengers);

// map() - similar to forEach however it returns new array

let numbers = [25, 50, 30, 10, 5];

let mappedNumbers = numbers.map(function (number) {
        console.log(number);
        return number * 5;
})
console.log(mappedNumbers);

// every()
// - iterates over all the items and checks if all the elements passess a given condition

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function (age) {
        console.log(age);
        return age >= 18;
});
console.log(checkAllAdult);


// some()
// - iterates over all the items check if even at least one of the items in the array passes the condition. same as every(), it will return boolean

let examScores = [75, 80, 74, 71];

let checkPassing = examScores.some(function (score) {
        console.log(score);
        return score >= 75;
});
console.log(checkPassing);

// filter()
// - creates a new array that contains elemetns which passed a given condition

let numberArray = [500, 12, 120, 60, 6, 30];

let divisibleby5 = numberArray.filter(function (number) {
        return number % 5 === 0;
});
console.log(divisibleby5);


// find()

let registeredUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];

let foundUser = registeredUsernames.find(function (username) {
        // console.log(username);
        return username === 'sheWhoCode';
})
console.log(foundUser);


// .includes() - returns a boolean true if it finds a matching item in the array
// case-sensitive

let registeredEmails = [
        'johnnyPhoenix1991@gmail.com',
        'michaelKing@gmail.com',
        'pedro_himself@yahoo.com',
        'sheJonesSmith@gmail.com'
];

let doesEmailExist = registeredEmails.includes('michaelKing@gmail.com');
console.log(doesEmailExist);

// mini-activity

console.log('===== mini activity =====');

let findUser = registeredUsernames.find(function (username) {
        return username === 'mikeyTheKing2000';
});
console.log(findUser);

function inputEmail(email) {
        let isEmailTaken = registeredEmails.includes(email);
        if (isEmailTaken === true) {
                alert('Email already exist.');
        } else {
                alert('Email is available, proceed to registration.');
        }
};

inputEmail('johnnyPhoenix1991@gmail.com');





